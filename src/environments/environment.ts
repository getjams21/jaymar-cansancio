// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD0ahkyOAoCG8_zSm1k2eomoSo7ffgZ8aA",
    authDomain: "gus-test-49a1b.firebaseapp.com",
    databaseURL: "https://gus-test-49a1b.firebaseio.com",
    projectId: "gus-test-49a1b",
    storageBucket: "gus-test-49a1b.appspot.com",
    messagingSenderId: "878906757310",
    appId: "1:878906757310:web:bc29b981c1a061b9914eef",
    measurementId: "G-JKEH4VKFB9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

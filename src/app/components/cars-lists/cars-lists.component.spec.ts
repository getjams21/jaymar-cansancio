import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsListsComponent } from './cars-lists.component';

describe('CarsListsComponent', () => {
  let component: CarsListsComponent;
  let fixture: ComponentFixture<CarsListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarsListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

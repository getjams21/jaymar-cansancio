export interface Car {
    id: number;
    max_speed: number;
    make: string;
    engine_size: number;
    created_date: string;
}



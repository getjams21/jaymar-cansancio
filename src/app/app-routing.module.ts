import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInComponent } from './components/sign-in/sign-in.component';
import { CarsListsComponent } from './components/cars-lists/cars-lists.component';

const routes: Routes = [
  { path: '', redirectTo: '/login-in', pathMatch: 'full' },
  { path: 'login-in', component: SignInComponent },
  { path: 'cars-list', component: CarsListsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
